#include <stdint.h>
#include "asm-example.h"

int putc(int c)
{
	/* this is still bugged -- it only works on qemu */
	*(uint32_t *)0x101f1000 = c;
	return c;
}

void puts(const char *s)
{
	while (*s)
		putc(*(s++));
}

/* Needed by printf */
int strnlen(const char *s, int count)
{
	const char *sc;

	for (sc = s; count-- && *sc != '\0'; ++sc)
		/* nothing */;
	return sc - s;
}
