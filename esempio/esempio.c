#include <stdint.h>
#include "asm-example.h"
#include "pp-printf.h"

static uint32_t get_stack_pointer(void)
{
	uint32_t ret;

	asm("mov %0, sp @azz" : "=r" (ret));
	return ret;
}

static void print_sp(void)
{
	printf("Stack pointer: %08lx\n", (long) get_stack_pointer());
}

static void funzione(int a, int b, int c)
{
	print_sp();
}

void esempio(void)
{
	print_sp();
	funzione(2, 3, 4);
	print_sp();
	while (1) {
		static int i;

		printf("...\n");
		for(i = 0; i < 100 * 1000 * 1000; i++)
			asm ("" : : : "memory");
	}
}
