#include <stdarg.h>

/* These used to be pp_printf but i had to turn them to standard names */
extern int printf(const char *fmt, ...)
        __attribute__((format(printf,1,2)));

extern int sprintf(char *s, const char *fmt, ...)
        __attribute__((format(printf,2,3)));

extern int vprintf(const char *fmt, va_list args);

extern int vsprintf(char *buf, const char *, va_list);

/* This is what we rely on for output */
extern void puts(const char *s);
